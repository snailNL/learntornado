import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from tornado.options import define, options
define("port", default=8000, help="run on the given port", type=int)

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        greeting = self.get_argument("greeting", "'hello")
        self.write(greeting + ', friendly user!')

if __name__=="__main__":
    # 使用options模块解析命令行
    tornado.options.parse_command_line()
    # 创建了Application类的实类
    app = tornado.web.Application(handlers=[(r"/", IndexHandler)])
    # 重复被执行
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    # 在程序准备好接收HTTP请求后，我们创建一个Tornado的IOLoop的实例
    tornado.ioloop.IOLoop.instance().start()
