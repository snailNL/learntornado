import tornado.ioloop
import tornado.web

class MainHandler(tornado.web.RequestHandler):

    '''
    # hello world
    def get(self):
        self.write("hellow, world")

    # show in the screen
    def get(self):
        self.write("You requested the main page.")
    '''

    # get the input html
    def get(self):
        self.write('<html><body><form action="/" method="post">'
                   '<input type="text" name="message">'
                   '<input type="submit" value="Submit">'
                   '</form></body></html>')
        if not self.get_cookie("mycookie"):
            self.set_cookie("mycookie", "myvalue")
            self.write("Your cookie was not set yet!")
        else:
            self.write("Your cookie was set!")

    # when input content from html and turn to, try get_argument("message")
    def post(self):
        self.set_header("Content-Type", "text/plain")
        self.write("You wrote " + self.get_argument("message"))



class StoryHandler(tornado.web.RequestHandler):
    def get(self, story_id):
        self.write("YOu requested the story " + story_id)

application = tornado.web.Application([
    (r"/", MainHandler),
    (r"/story/([0-9]+)", StoryHandler),
    cookie_secret="61oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo="
])

if __name__=="__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

